window.requestAnimFrame = (function(){
	return window.requestAnimFrame||
	 		window.webkitRequestAnimationFrame||
	 		window.mozRequestAnimationFrame||
	 		function(callback){
	 			window.setTimeout(callback,0.5);
	 		}

})();

var canvas = document.getElementById('canvas'),
    textcas = document.getElementById('textcas'),
	ctx = document.getElementById('canvas').getContext('2d');
	cw = window.innerWidth,
	ch = window.innerHeight,
	fireWorks = [],
	particles = [],
	limitierTotal = 5,
	limitierTick = 0,
	timerTotal = 10,
	timerTick = 0;
	mousedown = false;
var mx,my;

canvas.width = cw;
canvas.height = ch;

function Random(min,max){
	return Math.random()*(max-min)+min;	
}

function CalculateDistance(p1x,p1y,p2x,p2y){
	var xDistance = p1x - p2x;
	var yDistance = p1y - p2y;
	return Math.sqrt(Math.pow(xDistance,2)+Math.pow(yDistance,2));
}

function FireWork(sx,sy,tx,ty){
	this.x = sx;
	this.y = sy;
	this.sx = sx;
	this.sy = sy;
	this.tx = tx;
	this.ty = ty;

	this.hue = Random(0,360);

	this.distanceToTarget = CalculateDistance(sx,sy,tx,ty);
	this.distanceTraveled = 0;
	this.coordinates = [];
	this.coordinateCount = 3;

	while(this.coordinateCount--){
		this.coordinates.push([this.x,this.y]);
	}

	this.angle = Math.atan2(ty-sy,tx-sx);
	this.speed = 2;
	this.acceleration = 1.05;
	this.brightness = Random(50,70);
	this.targetRadius = 1;
}

FireWork.prototype.update = function(index){
	this.coordinates.pop();
	this.coordinates.unshift([this.x,this.y]);

	if(this.targetRadius < 8 ){
		this.targetRadius += 0.3;
	}else{
		this.targetRadius = 1;
	}

	this.speed *= this.acceleration;

	var vx = Math.cos(this.angle)*this.speed;
		vy = Math.sin(this.angle)*this.speed;

	this.distanceTraveled = CalculateDistance(this.sx,this.sy,this.x+vx,this.y+vy);

	if(this.distanceTraveled >= this.distanceToTarget){
		createParticle(this.tx,this.ty);
		fireWorks.splice(index,1);
	}else{
		this.x += vx;
		this.y += vy;
	}
}

FireWork.prototype.draw = function(){
	ctx.beginPath();
	ctx.moveTo(this.coordinates[this.coordinates.length-1][0],this.coordinates[this.coordinates.length-1][1]);
	ctx.lineTo(this.x,this.y);
	ctx.strokeStyle = 'hsl('+this.hue+',100%,'+this.brightness+'%)';
	ctx.stroke();

	ctx.beginPath();
	// draw the target for this firework with a pulsing circle
	ctx.arc( this.tx, this.ty, this.targetRadius, 0, Math.PI * 2 );
	ctx.stroke();
}

function Particle(x,y){
	this.x = x;
	this.y = y;

	this.coordinates = [];
	this.coordinateCount = 5;
	while(this.coordinateCount--){
		this.coordinates.push([this.x,this.y]);	
	}

	this.angle = Random(0,Math.PI*2);
	this.speed = Random(1,10);

	this.friction = 0.95;
	this.gravity = 1;
	// this.hue = Random(hue-20,hue+20);
	this.huep = Random(0,360);
	this.brightness = Random(50,80);
	this.alpha = 1;
	this.decay = Random(0.015,0.03);
}

Particle.prototype.update = function(index){
	this.coordinates.pop();
	this.coordinates.unshift([this.x,this.y]);
	this.speed *= this.friction;

	this.x += Math.cos(this.angle)*this.speed;
	this.y += Math.sin(this.angle)*this.speed + this.gravity;

	this.alpha -= this.decay;

	if(this.alpha <= this.decay){
		particles.splice(index,1);
	}
}

Particle.prototype.draw = function(){
	ctx.beginPath();
	ctx.moveTo(this.coordinates[this.coordinates.length-1][0],this.coordinates[this.coordinates.length-1][1]);
	ctx.lineTo(this.x,this.y);
	ctx.strokeStyle = 'hsl('+this.huep-20+',100%,'+this.brightness+'%,'+this.alpha+')';
	ctx.stroke();
}

function createParticle(x,y){
	var particleCount = 15;
	while(particleCount--){
		particles.push(new Particle(x,y));
	}
}

canvas.addEventListener("mousemove",function(e){
	mx = e.pageX - canvas.offsetLeft;
	my = e.pageY - canvas.offsetTop;
});

canvas.addEventListener('mousedown',function(e){
	e.preventDefault();
	mousedown = true;
});

canvas.addEventListener('mouseup',function(e){
	e.preventDefault();
	mousedown = false;
});

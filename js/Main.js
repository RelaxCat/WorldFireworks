var coordinate = function(x,y){
	this.x = x*5;
	this.y = y*12;
}

var singleName = function(){
	this.coordinates = new Array();	
}


var textctx = textcas.getContext('2d');
	width = textcas.width,
	height = textcas.height;
textctx.fillStyle = "#FF0000";
textctx.font = "20px Arial";
textctx.fillText("ME",10,35);

var pixs = textctx.getImageData(0,0,width,height).data;
var Pixels = new Array();

for(var i=0;i<pixs.length;i+= 4)
{
	var r = pixs[i],
		g = pixs[i+1],
		b = pixs[i+2],
		a = pixs[i+3];

	if(r != 0 || g != 0 || b != 0 ){
		var x = i%400;
		var y = i/400;
		Pixels.push(new coordinate(x,y));
	}
}

var nameS = new singleName();
nameS.coordinates = Pixels;
var Name = new Array();
Name.push(nameS);

function loop(){
	requestAnimFrame(loop);

	ctx.globalCompositeOperation = 'destination-out';
	ctx.fillStyle = 'rgba(0,0,0,0.5)';
	ctx.fillRect(0,0,cw,ch);
	ctx.globalCompositeOperation = 'lighter';

	var i = fireWorks.length;
	while(i--){
		fireWorks[i].draw();
		fireWorks[i].update(i);
	}

	var j = particles.length;
	while(j--){
		particles[j].draw();
		particles[j].update(i);
	}	

	if(timerTick>=timerTotal){
		if(!mousedown){
			if(Name.length>0){
				var single = Name[0];
				for(var i = 0;i<single.coordinates.length ;i++){
					var coordinate = single.coordinates[i];
					fireWorks.push(new FireWork(coordinate.x,ch,coordinate.x,coordinate.y));
				}
				Name.splice(0,1);
			}
			timerTick = 0;
		}
	}else{
		timerTick++;
	}


	if(limitierTick >= limitierTotal){
		if(mousedown){
			fireWorks.push(new FireWork(mx,ch,mx,my));
			limitierTick = 0;
			console.log("mx:"+mx+";my:"+my);
		}
	}else{
		limitierTick ++ ;
	}
}

window.onload = loop;